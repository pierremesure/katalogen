## Förslag på ny programvara

<!-- Exempel från katalogen, ersätt med Name, Url, Description, Keyword och User. -->

Name: Alfresco
Url: https://www.alfresco.com/ecm-software/alfresco-community-editions
Description:
+ Alfresco är en open source-plattform som används för att
+ hantera innehåll för en organisation. Det erbjuder en rad
+ funktioner som dokumenthantering, samarbete, delning och
+ spårning av innehåll.
Keyword: cms
User: vgr_r

/label ~programvara
