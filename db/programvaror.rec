%rec: Programvara
%key: Id
%auto: Id
%typedef: Keyword_t enum
+ cms
+ e-service
+ arkiv
+ integration
+ devops
+ kommunikation
+ operativsystem
+ utvecklingsplattform
+ datadelning
+ iot
+ integration
+ security
+ e-learning
+ geodata
+ databas
+ search
+ crm
+ webbanalys
+ ramverk
%type: Namn,Url line
%type: Keyword Keyword_t
%type: User rec Organisation
%mandatory: Name User Url
%sort: Id
%doc:
+ En öppen programvara som används av en offentlig organisation.

Id: 1
Name: Alfresco
Url: https://www.alfresco.com/ecm-software/alfresco-community-editions
Description:
+ Alfresco är en open source-plattform som används för att
+ hantera innehåll för en organisation. Det erbjuder en rad
+ funktioner som dokumenthantering, samarbete, delning och
+ spårning av innehåll.
Keyword: cms
User: vgr_r
User: havochvatten
User: svenska_kraftn
User: alingsas_k
User: lund_uh
User: smhi

Id: 2
Name: Anmäl hinder
Url: https://github.com/GoteborgsStad/anmalhinder-android
Description:
+ E-tjänst för att rapportera tillgänglighetshinder.
+ Tillhandahålls av Göteborgs stad.
Keyword: e-service
Publiccode: Yes
User: gbg_k

Id: 3
Name: Ansible
Url: https://github.com/ansible/ansible
Description:
+ Ansible är en mjukvaruplattform som används för automatisering
+ av IT-uppgifter, inklusive distribution av programvaror,
+ konfiguration och övervakning av system inom IT-infrastruktur.
Keyword: arkiv
User: lfv
User: orebro_r

Id: 4
Name: Apache Camel
Url: https://github.com/apache/camel
Description:
+ Apache Camel används inom integration för att ansluta
+ olika system och applikationer på ett effektivt sätt.
+ Tillhandahåller ett brett utbud av mönster och
+ komponenter för att stödja olika integrationsbehov.
Keyword: integration
User: lfv

Id: 5
Name: Archivematica
Url: https://www.archivematica.org/
Description:
+ Archivematica är ett system som används för digital bevarande
+ och bevarande av digitala samlingar. Det tillåter användare
+ att automatisera processerna för att bevara digitala arkiv,
+ inklusive säkerhetskopiering, lagring och tillgångshantering.
+ För de som vill ha långsiktig tillgång till pålitligt, autentiskt
+ och tillförlitligt digitalt innehåll.
Keyword: arkiv
User: sgu
User: kb
#User: AOAIS-nätverket

Id: 6
Name: ArgoCD
Url: https://argoproj.github.io/cd/
Description:
+ ArgoCD är en kontinuerlig leveransplattform som används för att
+ automatisera distributionen av applikationer och infrastruktur
+ till moln- och Kubernetes-baserade miljöer. Möjliggör uppdateringar
+ av applikationer och infrastruktur utan avbrott i tjänsten.
+ Ett typiskt GitOps-verktyg för Kubernetes.
Keyword: devops
User: lfv
User: orebro_r

Id: 7
Name: AtoM
Url: https://www.accesstomemory.org/
Description:
+ AtoM (Access to Memory) är en webbapplikation som används för att
+ hantera och presentera arkivmaterial på ett tillgängligt sätt.
+ Det är en open source-plattform som tillåter användare att söka,
+ bläddra och visa digitala samlingar av arkivmaterial. Möjliggör
+ samarbete online med typiska funktioner som filhantering
+ och dokumenthantering.
Keyword: arkiv
User: sgu
User: kb

Id: 8
Name: BasicUse
Url: https://github.com/umea-kommun/BasicUse
Description:
+ E-tjänsteplattform utvecklad av Umeå kommun som bland annat används
+ av medborgare för att från sin telefon eller dator initiera ärenden
+ hos Umeå
Keyword: e-service
User: umea_k
ProducedBy: umea_k

Id: 9
Name: BigBlueButton
Url: https://bigbluebutton.org
Description:
+ BigBlueButton är en webbkonferensplattform som används för
+ digitala möten och webbseminarier. Det tillåter användare
+ att interagera med varandra i realtid och erbjuder
+ funktioner som skärmdelning, chatt och inspelning.
Keyword: kommunikation
User: lm
User: hs_uh

Id: 10
Name: Camunda
Url: https://camunda.com/
Description:
+ Camunda är en öppen källkodsbaserad plattform för hantering
+ av affärsprocesser (BPM) och beslut. Det är utformat för att
+ hjälpa organisationer att automatisera sina verksamhetsprocesser
+ genom att visualisera, modellera och automatisera sina arbetsflöden.
+ Innehåller en mängd olika verktyg och funktioner för BPM, inklusive
+ processmodellering, processövervakning, automatisk processbearbetning,
+ hantering av arbetsuppgifter och mycket mer.
Keyword: devops
User: sundsvall_k

Id: 11
Name: CentOS
Url: https://www.centos.org/
Description:
+ CentOS är en fri och öppen källkodsvariant av operativsystemet
+ Red Hat Enterprise Linux (RHEL). Det är en populär plattform
+ för serverdrift, webbserver och molntjänster.
Keyword: operativsystem
User: arbetsformedlingen
User: smhi
User: forsakringskassan
User: lfv

Id: 12
Name: Codeberg
Url: https://codeberg.org
Description:
+ Codeberg är en webbplattform för att hantera och dela programkod
+ med andra. Det är en open source-plattform som erbjuder funktioner
+ för att hantera projekt, versionshantering och samarbete mellan utvecklare.
+ GDPR-vänlig!
Keyword: utvecklingsplattform
User: nosad

Id: 14
Name: DCAT-AP-SE-Processorn
Url: https://github.com/DIGGSweden/DCAT-AP-SE-Processor
Description:
+ I syfte att hjälpa organisationer att synas på dataportalen,
+ har detta verktyg tagits fram för att kunna införlivas i
+ godtycklig CI/CD driven kedja eller köras separat. Verktyget
+ skapar en metadataspecifikation enligt kraven på dataportalen.
+ Tillhandahålls av DIGG i samarbete med Arbetsförmedlingen.
Keyword: datadelning
Publiccode: Yes
User: digg

Id: 15
Name: Decidim
Url: https://decidim.org
Description:
+ Decidim är en open source-plattform för att hantera och driva
+ demokratiska processer, såsom medborgarbudgetar och
+ deltagarbaserade beslut. Det tillåter användare att engagera
+ sig i samhället och delta i beslutsprocesser på ett öppet och
+ demokratiskt sätt. Folkomröstningar och planeringsprocesser
+ är ytterligare exempel.
Keyword: e-service
User: sthlm_k
User: gbg_k
User: botkyrka_k
User: helsingborg_k

Id: 16
Name: Diagrams.net
Url: https://www.diagrams.net
Description:
+ Diagrams.net (tidigare kallad Draw.io) är en webbapplikation
+ som används för att skapa diagram och flödesscheman. Det är
+ en open source-plattform som erbjuder en mängd olika mallar
+ och verktyg för att skapa professionella diagram.
Keyword: kommunikation
User: lfv
User: gotland_k
User: gotland_r

Id: 17
Name: Discourse
Url: https://www.discourse.org
Description:
+ Discourse är en open source-plattform för forum och diskussioner.
+ Det erbjuder ett modernt och användarvänligt gränssnitt för att
+ skapa och moderera forum, med stöd för anpassning av utseendet
+ och integrering med andra verktyg. Funktioner som kategorisering
+ och taggning av diskussioner, konfigurerbar åtkomstkontroll,
+ liveuppdateringar och aviseringar i realtid.
Keyword: kommunikation
User: arbetsformedlingen

Id: 18
Name: Diwise
Url: https://github.com/diwise
Description:
+ Diwise är en IoT-kommunikationsplattform för att ta emot
+ sensordata och reglera fysiska styrsystem som innebär att
+ verksamhetssystem frikopplas från både kommunikationsgränssnitt
+ och olika sensortyper. En IoT-plattform är en viktig komponent
+ i en smart stad. Är öppen källkod och använder öppna standarder.
Keyword: iot
User: sundsvall_k

Id: 19
Name: Django
Url: https://www.djangoproject.com/
Description:
+ Django är ett webbapplikationsramverk skrivet i Python som används
+ för att bygga skalbara och säkra webbapplikationer. Det erbjuder en
+ mängd inbyggda funktioner och verktyg för att hantera databaser,
+ autentisering, säkerhet och användarupplevelse. Syfte att du ska
+ kunna fokusera på att skriva din app utan att behöva uppfinna hjulet
+ på nytt.
Keyword: cms
User: kb

Id: 20
Name: Docker
Url: https://www.docker.com
Description:
+ Docker är en open source-plattform för kontainerisering av
+ applikationer. Det tillåter användare att paketera applikationer
+ och dess beroenden i isolerade containrar, vilket gör det enkelt
+ att distribuera och hantera applikationer i olika miljöer.
Keyword: devops
User: lfv
User: orebro_r

Id: 21
Name: Drupal
Url: https://www.drupal.org
Description:
+ Drupal är ett innehållshanteringssystem (Content Management System)
+ som används för att bygga webbplatser och applikationer. Det erbjuder
+ en mängd inbyggda funktioner och verktyg för att hantera innehåll,
+ användarautentisering och anpassning av utseende och funktionalitet.
+ Flera benämner det som en digital upplevelseplattform (DXP) för webben.
Keyword: cms
User: alingsas_k
User: verksamt
User: gu_uh

Id: 22
Name: EJBCA
Url: https://www.ejbca.org
Description:
+ EJBCA är en plattform för certifikat- och nyckelhantering. Det tillåter
+ användare att skapa, distribuera och hantera digitala certifikat och
+ nycklar för en säker och skalbar miljö.
Keyword: security
User: arbetsformedlingen
User: rps

Id: 23
Name: ESSArch
Url: https://www.essarch.org
Description:
+ ESSArch är en plattform för digitalt bevarande av arkiv och samlingar.
+ Det erbjuder funktioner för långsiktig bevarande och tillgänglighet av
+ digitala samlingar, inklusive säkerhetskopiering, metadatahantering och
+ lagring i arkivstandardformat. Syftet är långsiktigt digitalt bevarande
+ på ett kostnadseffektivt och pålitlig sätt. Stödjer både disk, band och
+ molnbaserad lagring. Bygger på öppna standarder och api:er finns.
Keyword: arkiv
User: riksarkivet
User: sydarkivera
User: kyrkan

Id: 24
Name: Element
Url: https://element.io
Description:
+ Element (tidigare Riot) är en plattform för kommunikation och samarbete
+ inom grupper och organisationer. Det tillåter användare att skicka
+ meddelanden, samtala i röst- och videosamtal och dela filer, med stöd
+ för kryptering och integration med andra verktyg. Försäkringskassan
+ överväger att erbjuda Element som tjänst inom ramarna för regeringsuppdraget
+ , Samordnad och säker statlig it-drift.
Keyword: kommunikation
User: fk

Id: 25
Name: EntryScape
Url: https://bitbucket.org/metasolutions/entryscape/src/develop/
Description:
+ En applikation för informationshantering i öppen källkod med modulen
+  workbench för hantering av dokument. I informationssamlingar,
+ allmänna handlingar; offentliga handlingar och modulen Terms 
+ för taxonomier. Terminologier (begrepp och termer).
Keyword: datadelning
User: havochvatten
User: natur
User: skatteverket
User: vgr_r
User: blekinge_r
User: uppsala_k
User: digg
User: scb
User: lst

Id: 26
Name: EntryScape Catalog
Url: https://entryscape.com/catalog
Description:
+ En katalogapplikation i öppen källkod för att katalogisera;
+ beskriva; publicera; dela och sprida geodata till dataportaler.
Keyword: datadelning
User: havochvatten
User: huddinge_k
User: gavle_k
User: helsingborg_k

Id: 27
Name: FixMyStreet
Url: https://github.com/mysociety/fixmystreet
Description:
+ System för att rapportera, visa, eller diskutera
+ lokala problem. https://fixamingata.se/
Keyword: e-service
User: gbg_k

Id: 28
Name: Geonetwork
Url: https://geonetwork-opensource.org/
Description:
+ En öppen katalogapplikation för att katalogisera geodata.
Keyword: geodata
User: Länsstyrelsernas geodatakaloger
User: Geodataportalen

Id: 29
Name: Geoserver
Url: http://geoserver.org/
Description:
+ En server med öppen källkod för att dela ut geodata.
+ Både som kartbilder och vektordata.
Keyword: geodata
User: lfv

Id: 30
Name: Gitlab
Url: https://www.gitlab.com
Description:
+ GitLab är en webbaserad plattform för hantering av git-repositorier
+ och en mängd andra funktioner för utvecklingsprocessen. Det inkluderar
+ funktioner för kontinuerlig integration och kontinuerlig leverans (CI/CD),
+ projektledning, felhantering och samarbete.
Keyword: utvecklingsplattform
User: smhi
User: arbetsformedlingen
User: lfv
User: orebro_r

Id: 31
Name: Grafana
Url: https://github.com/grafana/grafana
Description:
+ Grafana är en open source-plattform för dataanalys och visualisering.
+ Det används för att skapa interaktiva dashboards och diagram baserade
+ på data från olika källor, inklusive databaser, molntjänster och IoT-enheter.
+ Exempel på loggkällor som stöds är Prometheus, Loki, Elasticsearch, InfluxDB,
+ Postgres och många fler.
Keyword: devops
User: lfv
User: smhi
User: orebro_r

Id: 32
Name: Gravitee
Url: https://www.gravitee.io
Description:
+ Gravitee är en API-hanterare. Det erbjuder en mängd funktioner för
+ att hantera, säkra och övervaka API:er, inklusive hantering av
+ API-nycklar, autentisering, hantering av trafik och analytics.
Keyword: integration
User: pension

Id: 33
Name: HAJK
Url: https://github.com/hajkmap/Hajk
Description:
+ En öppen kartplattform för att utveckla och publicera kartor och geodata.
+ Byggd på OpenLayers.
Keyword: datadelning
User: halmstad_k

Id: 34
Name: HashiCorp Vault
Url: https://www.vaultproject.io
Description:
+ Säkra, lagra och kontrollera åtkomsten till tokens, lösenord, certifikat,
+ krypteringsnycklar för att skydda hemligheter och annan känslig data med
+ hjälp av ett UI, CLI eller HTTP API.
Keyword: security
User: lfv
User: skatteverket

Id: 35
Name: Helm
Url: https://helm.sh
Description:
+ Helm är en open source-plattform för hantering av Kubernetes-paket.
+ Det används för att paketera, distribuera och hantera applikationer
+ i Kubernetes-miljöer, med stöd för anpassning av installation och
+ uppdatering av applikationer.
Keyword: devops
User: sthlm_bib
User: lfv
User: orebro_r

Id: 36
Name: Jenkins
Url: https://www.jenkins.io
Description:
+ Automationsserver som gör det möjligt för utvecklare över hela
+ världen att på ett tillförlitligt sätt bygga, testa och
+ distribuera sin programvara.
Keyword: devops
User: sthlm_bib

Id: 37
Name: Jitsi
Url: https://jitsi.org
Description:
+ Jitsi är en plattform för videokonferenser och samarbete.
+ Det erbjuder en mängd funktioner för att organisera och
+ genomföra videomöten, inklusive delning av skärm och filer,
+ inspelning av möten och möjligheten att integrera med andra
+ verktyg. Kan installeras på egen infrastruktur eller användas
+ via molnleverantörer.
Keyword: kommunikation
User: arbetsformedlingen
User: kassan
User: lfv
User: boverket
User: elverket
User: havochvatten
User: sthlmbrand

Id: 38
Name: Kafka
Url: https://kafka.apache.org/
Description:
+ Kafka är en distribuerad plattform som används för att hantera
+ stora volymer av strömmande data i realtid. Plattformen kan
+ hantera både strömmande data och batchdata och är utformad för
+ att skalas upp och hantera stora datavolymer i realtid.
Keyword: integration
User: sthlm_bib

Id: 39
Name: KeePassXC
Url: https://keepassxc.org/
Description:
+ KeePassXC är en plattform för hantering av lösenord som är utformad
+ för att vara säker, användarvänlig och lättanvänd. Programvaran
+ gör det möjligt att generera, lagra och hantera lösenord på ett
+ säkert sätt, vilket minskar risken för dataintrång och identitetsstöld.
Keyword: security
User: adda
User: lfv

Id: 40
Name: Koha
Url: https://koha-community.org
Description:
+ Webbaserat bibliotekssystem med en SQL databas i grunden
+ (MySQL rekommenderas) med katalogdata som lagras i MARC-format
+ och som är tillgänglig via Z39.50 eller SRU. Användargränssnittet
+  är till hög grad konfigurerbart och adaptivt och har översatts
+ till flera olika språk. Koha har de flesta egenskaper som kan
+ behövas i ett bibliotekssystem.
Keyword: arkiv
User: ["Svenska Koha-nätverket"]
User: gotland_k
User: gotland_r

Id: 41
Name: Kubernetes
Url: https://kubernetes.io/
Description:
+ Kubernetes är en populär lösning för att hantera containrar i en mängd
+ olika användningsfall, inklusive molninfrastruktur, DevOps och automatiserad
+ hantering av applikationer. Plattformen är också känd för att vara flexibel
+ och anpassningsbar, vilket gör den till en användbar lösning för en mängd
+ olika applikationer och användare. Kubernetes, även känd som K8s, har blivit
+ en defactostandard för att automatisera distribution, skalning och hantering
+ av containeriserade applikationer.
Keyword: devops
User: sthlm_bib
User: lfv
User: orebro_r

Id: 42
Name: LibreOffice
Url: https://www.libreoffice.org
Description:
+ LibreOffice är en svit av produktivitetsverktyg som inkluderar textbehandling,
+ kalkylblad, presentationsprogram och andra verktyg för dokumenthantering.
+ Plattformen är utformad för att vara användarvänlig, lättanvänd och kompatibel
+ med en mängd olika filformat.
Keyword: kommunikation
User: smhi

Id: 43
Name: Linux
Url: Linux
Description:
+ Linux är ett open source-operativsystem som används på en mängd olika
+ datorer och enheter, inklusive servrar, persondatorer och mobila enheter.
+ Operativsystemet är utformat för att vara stabilt, säkert och anpassningsbart,
+ vilket gör det till en populär lösning för både personligt och professionellt
+ bruk.
Keyword: operativsystem
User: arbetsformedlingen
User: smhi
User: fk
User: lfv
User: orebro_r

Id: 44
Name: Matomo
Url: https://matomo.org
Description:
+ Matomo är en open webbanalysplattform som används för att spåra och analysera
+ besökarbeteende på webbplatser. Plattformen är utformad för att vara en säker,
+ flexibel och användarvänlig lösning för att samla in och analysera data från
+ webbplatser.
Keyword: webbanalys
User: kb
User: skatteverket
User: trr
User: lararforbundet
User: fk
User: digg
User: sundsvall_k
User: arbetsmiljo
User: botkyrka_k
User: statenskultur
User: lakemedelsv
User: msb
User: natur
User: sgu
User: transports
User: konsumentverket
User: pension
User: fohm

Id: 45
Name: Mattermost
Url: https://mattermost.com
Description:
+ Mattermost är ett chattverktyg för att möjliggöra kommunikation och samarbete
+ mellan team och organisationer. Utformad för att vara säker, skalbar och
+ anpassningsbar och ger användare möjlighet att kommunicera i realtid via text,
+ video och röst.
Keyword: kommunikation
User: arbetsformedlingen
User: smhi
User: sthlm_bib
User: pension
User: lm
User: trafikverket
User: lfv

Id: 46
Name: MediaWiki
Url: https://www.mediawiki.org/wiki/MediaWiki
Description:
+ MediaWiki är en wikiplattform som används för att skapa, organisera
+ och dela kunskap och information. Plattformen är också känd för sin
+ möjlighet att anpassa och utöka funktionaliteten genom plugins.
Keyword: kommunikation
User: smhi
User: gotland_k

Id: 47
Name: Moodle
Url: https://moodle.org
Description:
+ Moodle är en plattform för inlärning och utbildning som används för
+ att skapa och hantera online-kurser och innehåll. Plattformen ger
+ lärare och instruktörer möjlighet att skapa anpassade inlärningsupplevelser
+ för sina elever.
Keyword: e-learning
User: simrishamn_k
User: sodertalje_komvux
User: forsvaret
User: lnu_uh
User: tullverket

Id: 48
Name: MuleSoft Anypoint Platform
Url: https://www.mulesoft.com
Description:
+ MuleSoft Anypoint är känd för sin omfattande funktionalitet och är
+ särskilt framträdande som en API-plattform, med funktioner för design,
+ hantering och övervakning. Plattformen stöder också integration med
+ molntjänster och appar för att förenkla
+ utvecklings- och integreringsprocesserna.
Keyword: integration
User: jonkoping_r
User: uppsala_r
User: storsthlm
User: kau_uh
User: kau_uh
User: skatteverket
User: arbetsformedlingen
User: vgr_r
User: gotland_k
User: gotland_r

Id: 49
Name: NetarchiveSuite
Url: https://sbforge.org/display/NAS/NetarchiveSuite
Description: Verktyg för insamling och arkivering av hemsidor.
Keyword: arkiv
User: kb

Id: 50
Name: Nextcloud
Url: https://github.com/nextcloud
Description:
+ Nextcloud är en molnbaserad plattform för filsynkronisering och
+ samarbete som ger användare möjlighet att lagra och dela filer på
+ ett säkert och enkelt sätt. Plattformen innehåller också en mängd
+ samarbetsverktyg som kalender, kontakter och meddelanden för att
+ underlätta samarbete mellan användare. Kan användas på egen hårdvara
+ eller i molnet. Plattformen är speciellt användbar för organisationer
+ som vill ha full kontroll över sin data.
Keyword: kommunikation
User: smhi
User: sjov
User: fk
User: trafikverket
User: svt
User: digg
User: fk
User: transports
User: universitetskansler
User: kemikalieinspektionen
User: riksrevisionen
User: sthlmbrand
User: boverket

Id: 51
Name: NodeBB
Url: https://nodebb.org/
Description:
+ NodeBB är en programvara för diskussionsforum som bygger på Node.js
+ och MongoDB. Finns ett brett utbud av plugins och teman.
Keyword: kommunikation
User: digg

Id: 52
Name: Odoo
Url: https://www.odoo.com
Description:
+ Svit för CRM, e-handel, redovisning, lager, försäljningsställen,
+ projektledning, etc.
Keyword: crm
User: arbetsformedlingen

Id: 53
Name: Open ePlatform
Url: http://www.oeplatform.org
Description:
+ eTjänstplattform som möjliggör för organisationer att konstruera och
+ publicera e-tjänster till sina besökare; samt låta besökarna följa
+ ärenden mot verksamheten via Mina sidor. Bygger i sin tur på OpenHierarchy.
Keyword: e-service
User: sundsvall_k
User: finspang_k
User: mjolby_k
User: norrkoping_k
User: soderkoping_k
User: vadstena_k
User: vastervik_k
User: valdermarsvik_k

Id: 54
Name: OpenStreetMap
Url: https://wiki.openstreetmap.org/wiki/API
Description:
+ OpenStreetMap (OSM) är ett öppet samarbetsprojekt för att
+ skapa en fritt redigerbar världskarta.
Keyword: geodata
User: smhi
User: gotland_k
User: gotland_r

Id: 55
Name: OrigoMap
Url: https://github.com/origo-map/origo
Description:
+ En öppen kartplattform för att utveckla och publicera
+ kartor och geodata. Byggd på OpenLayers.
Keyword: datadelning
User: sundsvall_k
User: enkoping_k

Id: 56
Name: PostgreSQL - PostGIS
Url: https://postgis.net/
Description:
+ PostGIS är ett databastillägg för att lagra och arbeta
+ med geodata i PostgreSQL.
Keyword: geodata
User: smhi

Id: 57
Name: PostgreSQL relationsdatabas
Url: http://postgresql.org
Description:
+ PostgreSQL är en relationsdatabas som är känd för sin pålitlighet,
+ skalbarhet och flexibilitet. Används av väldigt många organisationer.
Keyword: databas
User: smhi
User: lfv
User: sjv
User: vgr_r
User: migra
User: domstolsverket
User: ki_uh
User: sundsvall_k
User: raa
User: brottsforebyggande

Id: 58
Name: Prometheus
Url: https://prometheus.io
Description: Verktyg för monitorering
Keyword: devops
User: lfv
User: sthlm_bib
User: orebro_r

Id: 59
Name: Puppet
Url: https://puppetlabs.com
Description:
+ Puppet är populär för IT-automation. Modernisera, hantera och
+ anpassa infrastruktur genom kontinuerlig automatisering.
Keyword: devops
User: skolverket
User: skatteverket
User: arbetsformedlingen
User: pension
User: smhi

Id: 60
Name: PxWeb
Url: https://www.scb.se/vara-tjanster/statistikprogram-for-px-filer/PxWeb/
Description:
+ PxWeb är ett program för publicering av statistik. Tillhandahålls av SCB.
Keyword: datadelning
Publiccode: Yes
User: scb

Id: 61
Name: QGIS
Url: https://www.qgis.org/en/site
Description:
+ Generellt geodata(GIS)-verktyg som kan analysera; bearbeta och visualisera
+ geodata från alla förekommande källor.
Keyword: geodata
User: smhi

Id: 62
Name: Red Hat Enterprise Linux
Url: https://www.redhat.com/en/technologies/linux-platforms/enterprise-linux
Description: Linuxdistribution
Keyword: operativsystem
User: lfv
User: smhi
User: fk
User: arbetsformedlingen
User: orebro_r

Id: 63
Name: Red Hat Fuse
Url: https://www.redhat.com/en/products/integration
Description:
+ Hjälper företag och organisationer att integrera olika system och
+ applikationer. Byggd på Apache Camel och levereras med en mängd
+ integrationskomponenter och verktyg som möjliggör snabb och
+ enkel utveckling av integrationslösningar.
Keyword: integration
User: pension
User: fk
User: tullverket

Id: 64
Name: Redmine
Url: https://www.redmine.org/
Description:
+ Webbaserad projektlednings- och problemspårningsverktyg. Det tillåter
+ användare att hantera flera projekt och tillhörande delprojekt.
+ Verktyget har wikis och forum per projekt, tidsspårning,
+ och rollbaserad åtkomstkontroll.
Keyword: kommunikation
User: sthlm_k

Id: 65
Url: https://www.rocket.chat/
Name: Rocket Chat
Description:
+ Rocket.Chat är en programvara för teamkommunikation som liknar andra populära
+ chattplattformar som Slack eller Microsoft Teams. Ger möjlighet till
+ gruppchatt, direktmeddelanden, delning av filer och skärmdelning.
Keyword: kommunikation
User: kb
User: boverket
User: elverket
User: havochvatten
User: sthlmbrand

Id: 66
Name: Roda
Url: https://www.roda-community.org
Description:
+ RODA (Repository of Authentic Digital Records) är en programvara som
+ används för att hantera e-arkiv. Utvecklad av det portugisiska
+ nationella arkivet och är baserad på ett modulärt system med
+ öppna standarder. Funktionalitet för alla huvudenheterna
+ i OAIS-referensmodellen.
Keyword: arkiv
User: tullverket
User: lfv
User: sjov

Id: 67
Name: Sambuh
Url: https://github.com/GoteborgsStad/boendeprocessAPP
Description:
+ En app för boende processen inom verksamheter i Göteborgs Stad.
+ Tillhandahålls av GöteborgsStad.
Keyword: e-service
User: gbg_k
ProducedBy: gbg_k

Id: 69
Name: Smartakartan
Url: https://github.com/GoteborgsStad/smartakartan3.0
Description:
+ Upptäck initiativ som gör det lätt att hyra, dela, byta, låna, ge och få!
Keyword: e-service
User: gbg_k
ProducedBy: gbg_k

Id: 70
Name: Solr
Url: https://solr.apache.org/
Description: Search platform. Sökplattform
Keyword: search
#User: ["Utbildningsförvaltningen","Stockholms statsbibliotek"]
User: sthlm_bib

Id: 71
Name: SonarQube
Url: https://github.com/SonarSource/sonarqube/
Description: Granskar kodkvalitet och kodsäkerhetsverktyg.
Keyword: devops
User: lvf

Id: 72
Name: Spring Boot
Url: https://spring.io/projects/spring-boot
Description: Ett ramverk för att förenkla applikationsutveckling.
Keyword: ramverk
User: lfv

Id: 73
Name: Strapi
Url: https://strapi.io/
Description:
+ Strapi är en headless CMS (Content Management System).
+ Erbjuder en enkel hantering av innehåll via ett
+ administrationsverktyg.
Keyword: cms
User: digg
User: su_uh

Id: 74
Name: Ubuntu
Url: https://ubuntu.com/
Description: Linuxdistribution
Keyword: operativsystem
User: smhi
User: fk
User: lfv

Id: 75
Name: Varnish Cache
Url: https://github.com/varnishcache/varnish-cache
Description:
+ Varnish Cache är en proxy-cache som används för att förbättra
+ prestandan och hastigheten på webbplatser och applikationer
+ genom att lagra och servera cachade webbsidor och innehåll.
+ Plattformen är utformad för att hantera en hög belastning
+ och för att minska belastningen på webbservern genom att
+ svara på förfrågningar från cacheminnet.
Keyword: devops
User: arbetsformedlingen
User: smhi

Id: 76
Name: WSO2 API Manager
Url: https://wso2.com/api-management
Description:
+ WSO2 API Manager möjliggör hantering och övervakning av API:er
+ och deras livscykel. Funktioner för att stödja arbetet med
+ versionering, dokumentation, testning, övervakning och säkerhet.
Keyword: integration
User: vtrafik
User: lm
User: havochvatten
User: csn
User: bolagsverket
User: fk
User: sundsvall_k
User: lst
User: tillvaxtverket
User: natur

Id: 77
Name: WordPress
Url: https://wordpress.org
Description:
+ Content Management System skrivet i PHP och som använder
+ databasen MySQL för datalagringen
Keyword: cms
User: helsingborg_k
User: eslov_k
User: digg
ProducedBy: helsingborg_k

Id: 78
Name: Collabora
Url: https://www.collaboraoffice.com/
Description:
+ Collabora Office är en molnbaserad mjukvarusvit som ger användare möjlighet
+ att skapa, redigera och dela dokument, kalkylblad och presentationer på ett
+ säkert och effektivt sätt. Det är baserat på LibreOffice, men erbjuder extra
+ funktioner som utvecklats av Collabora Productivity, ett företag som
+ specialiserat sig på öppen källkodsbaserade produkter.
Keyword: kommunikation
User: universitetskansler
User: kemikalieinspektionen
User: boverket

Id: 79
Name: Alaveteli (Handlingar.se)
Url: https://github.com/mysociety/alaveteli
Description:
+ Effektivisera ärenden kring begäran om allmän handling enligt
+ offentlighetsprincipen eller data enligt datalagen mellan myndighet
+ och medborgare. Undvik att få samma fråga om allmän handling flera gånger
+ genom att redan gjorda frågor och svar med handlingar blir publicerade
+ och sökbara på plattformen och via sökmotorer på Internet. Se Handlingar.se.
Keyword: datadelning
User: openknowledgesweden

Id: 80
Name: Designsystem
Url: https://designsystem.arbetsformedlingen.se/
Description:
+ Arbetsförmedlingen och andra myndigheter samarbetar kring ett gemensamt
+ designsystem. I designsystemet hittar du teknik, design och verktyg baserat
+ på gemensamma insikter, research och användarbehov. Återanvänd allt arbete
+ fast med din grafiska profil.
Keyword: kommunikation
Publiccode: Yes
User: arbetsformedlingen
User: skolverket

Id: 81
Name: WeKan
Url: https://wekan.github.io/
Description:
+ Wekan är ett användbart verktyg för team som snabbt vill se hur arbetet
+ fortlöper enligt konceptet \"kanban-brädor\". Användare kan skapa och hantera
+ uppgifter på ett visuellt och intuitivt sätt genom att flytta kort på brädet
+ från en kolumn till en annan. Ett kostnadseffektivt och snabbt sätt att se
+ hur det går med projektet.
Keyword: kommunikation
User: alingsas_k

#Id: 13#Name: CollectiveAccess
#Url: https://collectiveaccess.org
#Description:
#+ Skapa kataloger som stämmer överens med dina behov utan anpassad programmering
#Keyword: arkiv
#User: ["Collective Access Sverige"]

# Har kommenterat bort screen9, hittar ej källkoden till produkten.
#Id: 68#Name: Screen9#Url: https://screen9.com/esam/
#Description:
#+ Screen9 är nordens ledande online plattform för videokommunikation.
#+ Ett verktyg för dig som vill ladda upp, hantera, redigera, publicera eller
#+ livesända video i en GDPR säkrad videoplattform som även uppfyller
#+ webbtillgänglighetsdirektivet.
#Keyword: kommunikation
#User: digg
#User: esam
